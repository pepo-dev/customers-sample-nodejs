var Datastore = require("nedb");

var db = new Datastore({ "filename": __dirname + '/../datastore/customerDB.json', autoload: true });
db.ensureIndex({ fieldName: 'document', unique: true }, function (err) {});

var customerDAO = {

    list: function (offset, limit) {

        var promise = new Promise(function(resolve, reject) {

            db.find({}).skip(offset).limit(limit).exec(function (err, doc) {

                if (err !== null) {
                    reject (err);
                } else {
                    resolve (doc);
                }

            });  

        });

        return promise;

    },

    findById: function (id) {

        var promise = new Promise(function(resolve, reject) {

            db.find ({"_id": id}, function (err, doc) {

                if (err !== null) {
                    reject (err);
                } else {
                    resolve (doc);
                }

            });  

        });

        return promise;

    },

    save: function (customer) {

        var promise = new Promise(function(resolve, reject) {

            self.findById(customer._id)
                .then(function(data) {

                    if (data.length > 0) {

                        db.update ({'_id': customer._id}, customer, {}, function (err, numReplaced) {

                            if (err !== null) {
                                reject (err);
                            } else {
                                resolve (customer);
                            }

                        });                    

                    } else {

                        db.insert (customer, function (err, doc) {

                            if (err !== null) {
                                reject (err);
                            } else {
                                resolve (doc);
                            }

                        });

                    }
                })
                .catch(function(error) {
                    reject (error);
                });

        });

        return promise;

    },

    remove: function (id) {

        var promise = new Promise(function(resolve, reject) {

            db.remove ({"_id": id}, function (err, doc) {

                if (err !== null) {
                    reject (err);
                } else {
                    resolve (doc);
                }

            });  

        });

        return promise;

    }

};

var self = customerDAO;

module.exports = self;