var os = require('os');
var express = require('express');
var bodyParser = require('body-parser');

var dao = require("./dao/customerDAO.js");

var app = express();
app.use(bodyParser.json());

// POST customers
app.post('/customers', function (req, res) {

    dao.save(req.body)
        .then(function(data) {
            res.header('Location', '/customers/' + data._id)
            res.status(201).end();
        })
        .catch(function(error) {
            res.status(500).send(
                {'message': 'Erro ao inserir o cliente.', 'error': error}).end();
        });

});

// GET all customers
app.get('/customers', function (req, res) {

    dao.list(req.query._offset, req.query._limit)
        .then(function(data) {
            res.status(200).send(data).end();
        })
        .catch(function(error) {
            res.status(500).send(
                {'message': 'Erro ao listar os clientes.', 'error': error}).end();
        });

});

// GET customer by ID
app.get('/customers/:id(\\w+)', function (req, res) {
	
    dao.findById(req.params.id)
        .then(function(data) {
            if (data.length > 0) {
                res.status(200).send(data[0]).end();
            } else {
                res.status(404).end();
            }
        })
        .catch(function(error) {
            res.status(500).send(
                {'message': 'Erro ao consultar o cliente ' + req.params.id + '.', 'error': error}).end();              
        });

});

// PUT customer by ID
app.put('/customers/:id(\\w+)', function (req, res) {
	
    req.body._id = req.params.id;

    dao.save(req.body)
        .then(function(data) {
            res.status(200).end();
        })
        .catch(function(error) {
            res.status(500).send(
                {'message': 'Erro ao alterar o cliente ' + req.params.id + '.', 'error': error}).end();

        });

});

// DELETE customer by ID
app.delete('/customers/:id(\\w+)', function (req, res) {

    dao.remove(req.params.id)
        .then(function(data) {
            res.status(200).end();
        })
        .catch(function(error) {
            res.status(500).send(
                {'message': 'Erro ao excluir o cliente ' + req.params.id + '.', 'error': error}).end();
        });

});

app.get('/info', function (req, res) {
    res.status(200).send('HOSTNAME: ' + os.hostname()).end();
});

app.listen(8080);
console.log('Express server started on port 8080!');